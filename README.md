 # Sprout Coding Challenge 

Selenium Webtest framework that uses the page object model to test the Sprout Social tweet functionality.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

Have credentials for authentication 
```bash
export SPROUT_USERNAME=<username>
export SPROUT_PASSWORD=<'password'>
```

### Installing

```bash 
brew install chromedriver
brew install gradle
git clone git@gitlab.com:nbob31/sprout-qa-webtest.git
./gradlew clean build 
```

## Running the tests
```Import into intellj and go to the SproutTest.java and run the tests or run with gradle.```

```bash
./gradlew test
```

### Break down into end to end tests

1. Create Tweet and view.
2. Create Tweet and reply.
3. Create and schedule Tweet and view on calendar.






