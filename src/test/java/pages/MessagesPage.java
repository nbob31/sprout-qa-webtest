package pages;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.modals.TweetModal;

public class MessagesPage extends BasePage {

    private static final By OLD_TO_NEW_BUTTON = By.cssSelector("[class='SelectInline-select']");
    private static final By REPLY_BUTTON = By.cssSelector("[data-qa-icon-button='Reply']");
    private static final By REPLY_TWEET_BOX = By.cssSelector("div.DraftEditor-editorContainer div.public-DraftEditor-content");
    private static final By TWEETED_TEXT = By.cssSelector("div.MessageText");
    private static final By AUTOMATION_INBOX = By.cssSelector("div.NavItemContent-text");
    private static final By COMPOSE_TWEET_BUTTON = By.cssSelector("a[class='menu-item-link'][href='#compose']");

    public MessagesPage(WebDriver driver) {
        super(driver);
        this.url = "https://app.sproutsocial.com/messages/smart/";
    }

    public boolean isCurrentPage() {
        boolean urlMatches = driver.getCurrentUrl() == this.url;
        WebElement old2NewButton = driver.findElement(OLD_TO_NEW_BUTTON);
        return urlMatches && old2NewButton != null;
    }

    public void clickReplyIcon() {
        Actions action = new Actions(driver);
        WebElement replyTweetIcon = driver.findElement(REPLY_BUTTON);
        action.moveToElement(replyTweetIcon).build().perform();
        driver.findElement(REPLY_BUTTON).click();
    }

    public void enterTweetReply(String replyTweet) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(TWEETED_TEXT));
        clickReplyIcon();

        driver.findElement(REPLY_TWEET_BOX).sendKeys(replyTweet);
    }

    public String getTweetedTextForReply() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        driver.navigate().refresh();
        WebElement tweetedMessageText = wait.until(ExpectedConditions.visibilityOfElementLocated(TWEETED_TEXT));
        return tweetedMessageText.getText();
    }

    public void clickAutomationInbox() {
        List<WebElement> navLinks = driver.findElements(AUTOMATION_INBOX);
        for (WebElement element: navLinks) {
            if (element.getText().equals("TestAutomation")) {
                element.click();
            }
        }
    }

    public TweetModal clickComposeButton() {
        driver.findElement(COMPOSE_TWEET_BUTTON).click();
        return new TweetModal(driver);
    }
}
