package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class LoginPage extends BasePage {

    private static final By EMAIL_INPUT_FIELD = By.cssSelector("input[id='signin-email']");
    private static final By PASSWORD_INPUT_FIELD = By.cssSelector("input[type='password']");
    private static final By LOGIN_BUTTON = By.cssSelector("button[type='submit']");

    public LoginPage(WebDriver driver) {
        super(driver);
        this.url = "https://app.sproutsocial.com/login";
    }

    public boolean isCurrentPage() {
        boolean urlMatches = driver.getCurrentUrl() == this.url;
        WebElement emailInputField = driver.findElement(EMAIL_INPUT_FIELD);
        WebElement passwordLoginButton = driver.findElement(PASSWORD_INPUT_FIELD);
        WebElement loginButton = driver.findElement(LOGIN_BUTTON);
        return urlMatches && emailInputField != null && passwordLoginButton != null && loginButton != null;
    }

    public void loginUser(String username, String password) {
        driver.findElement(EMAIL_INPUT_FIELD).sendKeys(username);
        driver.findElement(PASSWORD_INPUT_FIELD).sendKeys(password);
        driver.findElement(LOGIN_BUTTON).click();
        }

    }

