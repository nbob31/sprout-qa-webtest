package pages.modals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class DeleteModal {

    private static final By DELETE_BUTTON = By.cssSelector("a.action.warning-action.js-submit");

    public WebDriver driver;

    public DeleteModal(WebDriver driver) {
        this.driver = driver;
    }

    public void clickDeleteTweet() {
        driver.findElement(DELETE_BUTTON).click();
    }

}

