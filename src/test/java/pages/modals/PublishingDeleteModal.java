package pages.modals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PublishingDeleteModal {

    private static final By DELETE_MODAL_BUTTON = By.cssSelector("[class='Button _warning _default']");

    private WebElement deleteButton;

    public WebDriver driver;

    public PublishingDeleteModal(WebDriver driver) {
        this.driver = driver;
    }

    public void clickDeleteButton(){
        deleteButton = driver.findElement(DELETE_MODAL_BUTTON);
        deleteButton.click();
    }
}




