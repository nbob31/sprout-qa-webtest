package pages.modals;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TweetModal {

    private static final By TWEET_MODAL_INPUT_BOX = By.cssSelector("div.DraftEditor-editorContainer div.public-DraftEditor-content");
    private static final By SCHEDULE_TWEET_BUTTON = By.cssSelector("[data-qa-button-state='schedule_post']");
    private static final By SEND_TWEET_BUTTON = By.cssSelector("[data-qa-button-state='send_post']");
    private static final By PUBLISH_DROPDOWN = By.cssSelector("div[class='SelectInline-select-content']");
    private static final By SCHEDULE_MANUALLY_OPTION = By.cssSelector("[data-qa-menu-item='Schedule Manually']");
    private static final By PUBLISH_NOW_OPTION = By.cssSelector("[data-qa-menu-item='Publish Now']");
    private String CALENDAR_TOP = ("table.ui-datepicker-calendar");

    public WebDriver driver;


    public TweetModal(WebDriver driver) {
        this.driver = driver;
    }

    public TweetModal enterTweetText(String tweet) {
        driver.findElement(TWEET_MODAL_INPUT_BOX).sendKeys(tweet);
        driver.findElement(PUBLISH_DROPDOWN).click();
        clickPublishNowOption();
        driver.findElement(SEND_TWEET_BUTTON).click();
        return this;
    }

    // Months start at 0; august = 7 and september=8 for MM/DD/YY
    public void selectDateForTweet(String date) {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        driver.findElement(By.cssSelector("a.ui-datepicker-next.ui-corner-all")).click();
        By locator = By.cssSelector(CALENDAR_TOP + " td[data-qa-schedule-cell-date=\"" + date + "\"] > a");
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
        driver.findElement(locator).click();
    }

    public void clickPublishNowOption() {
        driver.findElement(PUBLISH_NOW_OPTION).click();
    }

    public TweetModal enterScheduledTweetText(String scheduledTweet, String tweetDay) {
        WebElement scheduledTweetText = driver.findElement(TWEET_MODAL_INPUT_BOX);
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        scheduledTweetText.sendKeys(scheduledTweet);
        WebElement publishDropdown = driver.findElement(PUBLISH_DROPDOWN);
        publishDropdown.click();
        driver.findElement(SCHEDULE_MANUALLY_OPTION).click();
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        selectDateForTweet(tweetDay);
        driver.findElement(SCHEDULE_TWEET_BUTTON).click();
        return this;
    }
}


