package pages;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pages.modals.DeleteModal;
import pages.modals.TweetModal;

public class FeedsPage extends BasePage {

    private static final By TWEETED_TEXT = By.cssSelector("span.js-toggle-message-text");
    private static final By COMPOSE_TWEET_BUTTON = By.cssSelector("a[class='menu-item-link'][href='#compose']");
    private static final By TWITTER_LIST_BOX = By.cssSelector("section[class='pillow']");
    private static final By TWEET_SETTING_ICON = By.cssSelector("div.message_actions");
    private static final By DELETE_TWEET_OPTION = By.cssSelector("a.delete");
    private static final By PUBLISHING_LINK = By.cssSelector("a[class='menu-item-link publishing'][href='/publishing/']");

    public FeedsPage(WebDriver driver) {
        super(driver);
        this.url = "https://app.sproutsocial.com/feeds/twitter/reader";
    }

    public boolean isCurrentPage() {
        boolean urlMatches = driver.getCurrentUrl() == this.url;
        WebElement twitterListTitle = driver.findElement(TWITTER_LIST_BOX);
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        return urlMatches && twitterListTitle.isDisplayed();
    }

    public boolean isTweetedMessageDisplayed() {
        WebElement tweetedMessageText = driver.findElement(TWEETED_TEXT);
        return tweetedMessageText.isDisplayed();
    }

    public String getTweetedText() {
        WebDriverWait wait = new WebDriverWait(driver, 10);
        driver.navigate().refresh();
        WebElement tweetedMessageText = wait.until(ExpectedConditions.visibilityOfElementLocated(TWEETED_TEXT));
        return tweetedMessageText.getText();
    }

    public TweetModal clickComposeButton() {
        driver.findElement(COMPOSE_TWEET_BUTTON).click();
        return new TweetModal(driver);
    }

    public DeleteModal clickDeleteTweetOption() {
        Actions action = new Actions(driver);
        WebElement tweetSettingIcon = driver.findElement(TWEET_SETTING_ICON);
        action.moveToElement(tweetSettingIcon).build().perform();
        tweetSettingIcon.click();
        driver.findElement(DELETE_TWEET_OPTION).click();
        return new DeleteModal(driver);
    }

    public PublishingPage clickPublishLink() {
        driver.findElement(PUBLISHING_LINK).click();
        return new PublishingPage(driver);
    }
}
