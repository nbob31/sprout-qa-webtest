package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pages.modals.PublishingDeleteModal;
import pages.modals.TweetModal;

public class PublishingPage extends BasePage {

    private static final By DELETE_TWEET_ICON = By.cssSelector("[data-qa-icon-button='Delete']");
    private static final By TWEETED_MESSAGE_TEXT = By.cssSelector("div[class='MessageText']");
    private static final By SCHEDULED_TWEET_LABEL = By.cssSelector("div.MessageMeta-text");
    private static final By CAL_DATE_TITLE = By.cssSelector("[class='PubLayout-title']");
    private static final By START_DATE_BOX = By.id("startDate");
    private static final By END_DATE_BOX = By.id("endDate");
    private static final By COMPOSE_TWEET_BUTTON = By.cssSelector("a[class='menu-item-link'][href='#compose']");
    private static final By LIST_BUTTON = By.cssSelector("[data-qa-calendar-list='true']");
    private static final By TWEET_BAR = By.cssSelector("[class='CalendarChart-bar  _isActive']");

    public PublishingPage(WebDriver driver) {
        super(driver);
        this.url = "https://app.sproutsocial.com/publishing/";
    }

    public boolean isCurrentPage() {
        boolean urlMatches = driver.getCurrentUrl() == this.url;
        WebElement calDateTitle = driver.findElement(CAL_DATE_TITLE);
        return urlMatches && calDateTitle != null;
    }

    // StartDate and EndDate must be MM/DD/YYYY
    public void setTweetDateRange(String startDateRange, String endDateRange) {
        WebElement startDateBox = driver.findElement(START_DATE_BOX);
        startDateBox.clear();
        startDateBox.sendKeys(startDateRange);
        WebElement endDateBox = driver.findElement(END_DATE_BOX);
        endDateBox.clear();
        endDateBox.sendKeys(endDateRange);
    }

    public String getTweetedText() {
        return driver.findElement(TWEETED_MESSAGE_TEXT).getText();
    }

    public Boolean isTweetedScheduleTextDisplayed() {
        return driver.findElement(TWEETED_MESSAGE_TEXT).isDisplayed();
    }

    public Boolean isScheduleMessageLabelDisplayed() {
        return driver.findElement(SCHEDULED_TWEET_LABEL).isDisplayed();
    }

    public PublishingDeleteModal clickDeleteTweetIcon() {
        WebElement deleteTweetIcon = driver.findElement(DELETE_TWEET_ICON);
        deleteTweetIcon.click();
        return new PublishingDeleteModal(driver);
    }

    public TweetModal clickComposeButton() {
        driver.findElement(COMPOSE_TWEET_BUTTON).click();
        return new TweetModal(driver);
    }

    public void clickCalTweetBar() {
        driver.findElement(TWEET_BAR).click();
    }

    public void clickListButton() {
        driver.findElement(LIST_BUTTON).click();

    }

}