package pages;

import org.openqa.selenium.WebDriver;

public abstract class BasePage {


    public WebDriver driver;

    public BasePage(WebDriver driver) {
        System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver");
        this.driver = driver;
        driver.manage().window().maximize();
    }

    public String url;

    public abstract boolean isCurrentPage() throws InterruptedException;

    public void goToUrl() {
        this.driver.get(this.url);
    }

}
