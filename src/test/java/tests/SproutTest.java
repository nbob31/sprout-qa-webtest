package tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.FeedsPage;
import pages.LoginPage;
import pages.MessagesPage;
import pages.PublishingPage;
import pages.modals.DeleteModal;
import pages.modals.PublishingDeleteModal;
import pages.modals.TweetModal;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class SproutTest {
    private static final String tweetString = "Test Tweet " + new Date().getTime();
    private static final String replyTweetString = "Test Reply " + new Date().getTime();
    private static final String scheduleTweetString = "Test Schedule Tweet " + new Date().getTime();
    private static final String calDate = "7/12/2018";
    private WebDriver webDriver = new ChromeDriver();
    private FeedsPage feedsPage;
    private TweetModal tweetModal;
    private MessagesPage messagesPage;
    private PublishingPage publishingPage;
    private DeleteModal deleteModal;
    private PublishingDeleteModal publishingDeleteModal;

    @Before
    public void setUp() {
        feedsPage = new FeedsPage(webDriver);
        tweetModal = new TweetModal(webDriver);
        messagesPage = new MessagesPage(webDriver);
        publishingPage = new PublishingPage(webDriver);
        deleteModal = new DeleteModal(webDriver);
        publishingDeleteModal = new PublishingDeleteModal(webDriver);
        webDriver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
    }

    @After
    public void tearDown() {
        webDriver.close();
        webDriver.quit();
    }

    @Test
    public void CreateTwitterMessageTest() {
        webDriver.get(("https://app.sproutsocial.com/feeds/twitter/reader/"));
        LoginPage loginPage = new LoginPage(webDriver);
        loginPage.goToUrl();
        loginPage.loginUser(System.getenv("SPROUT_USERNAME"),System.getenv("SPROUT_PASSWORD"));
        feedsPage.clickComposeButton();
        tweetModal.enterTweetText(tweetString);
        assertEquals(tweetString, feedsPage.getTweetedText());
        assertTrue(feedsPage.isTweetedMessageDisplayed());
        feedsPage.clickDeleteTweetOption();
        deleteModal.clickDeleteTweet();
    }

    @Test
    public void ViewTweetsAndReplyTest() {
        webDriver.get(("https://app.sproutsocial.com/feeds/twitter/reader/"));
        LoginPage loginPage = new LoginPage(webDriver);
        loginPage.goToUrl();
        loginPage.loginUser(System.getenv("SPROUT_USERNAME"),System.getenv("SPROUT_PASSWORD"));
        feedsPage.clickComposeButton();
        tweetModal.enterTweetText(replyTweetString);
        webDriver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        webDriver.get("https://app.sproutsocial.com/messages/smart/");
        messagesPage.clickAutomationInbox();
        webDriver.navigate().refresh();
        assertEquals(replyTweetString, messagesPage.getTweetedTextForReply());
        feedsPage.goToUrl();
        feedsPage.clickDeleteTweetOption();
    }

    @Test
    public void ScheduleFutureTweetTest() {
        webDriver.get(("https://app.sproutsocial.com/publishing/"));
        LoginPage loginPage = new LoginPage(webDriver);
        loginPage.goToUrl();
        loginPage.loginUser(System.getenv("SPROUT_USERNAME"),System.getenv("SPROUT_PASSWORD"));
        publishingPage.clickComposeButton();
        tweetModal.enterScheduledTweetText(scheduleTweetString, calDate);
        publishingPage.clickListButton();
        webDriver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        publishingPage.setTweetDateRange("8/01/2018", "8/20/2018");
        assertEquals(scheduleTweetString, publishingPage.getTweetedText());
        assertTrue(publishingPage.isScheduleMessageLabelDisplayed());
        assertTrue(publishingPage.isTweetedScheduleTextDisplayed());
        publishingPage.clickDeleteTweetIcon();
        publishingDeleteModal.clickDeleteButton();
    }

}